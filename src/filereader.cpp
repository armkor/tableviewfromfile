#include "filereader.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QDebug>

void FileReader::read(UserInfoModel* model, const QString filename)
{
	if (model == nullptr)
		return;

	QFile file(filename);
	if(!file.open(QIODevice::ReadOnly)) {
		return;
	}

	QTextStream in(&file);
	while(!in.atEnd())
	{
		QString line = in.readLine();
		QStringList fields = line.split(";", QString::SkipEmptyParts);
		if (fields.size() != 3)
			continue;
		model->add( UserInfo( fields.at(0)
							, fields.at(1)
							, fields.at(2)));
	}

	file.close();
}
