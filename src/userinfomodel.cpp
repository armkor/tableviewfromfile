#include "userinfomodel.h"

void UserInfoModel::add(UserInfo value)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_userInfo << value;
	endInsertRows();
}

int UserInfoModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent)
	return m_userInfo.count();
}

QVariant UserInfoModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid() || !(index.row() >= 0 && index.row() < rowCount()))
		return QVariant();

	const UserInfo &item = m_userInfo[index.row()];

	if (role == IdRole)
		return item.id();

	if (role == NameRole)
		return item.name();

	if (role == PhoneNumberRole)
		return item.name();

	return QVariant();
}

QHash<int, QByteArray> UserInfoModel::roleNames() const
{
	QHash<int, QByteArray> roles;
	roles[IdRole] = "id";
	roles[NameRole] = "userName";
	roles[PhoneNumberRole] = "phoneNumber";
	return roles;
}

bool UserInfoModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (!index.isValid()  || !(index.row() >= 0 && index.row() < rowCount()))
		return false;

	if (role == IdRole)
	{
		m_userInfo[index.row()].setId(value.toString());
		emit dataChanged(index, index, QVector<int>{IdRole});
		return true;
	}

	if (role == NameRole)
	{
		m_userInfo[index.row()].setName(value.toString());
		emit dataChanged(index, index, QVector<int>{NameRole});
		return true;
	}

	if (role == PhoneNumberRole)
	{
		m_userInfo[index.row()].setPhoneNumber(value.toString());
		emit dataChanged(index, index, QVector<int>{PhoneNumberRole});
		return true;
	}

	return false;
}
