#pragma once
#include "userinfomodel.h"

class FileReader
{
public:
	static void read(UserInfoModel* model, const QString filename);
};

