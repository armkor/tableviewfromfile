import QtQuick 2.12
import QtQuick.Controls 1.4

ApplicationWindow {
    id: mainView
    visible: true
    width: 640
    height: 480
    title: qsTr("Scroll")

    ScrollView {
        anchors.fill: parent

        TableView {
            id: tableView
            height: mainView.height
            width: mainView.width

            TableViewColumn {
                role: "id"
                title: "id"
                width: mainView.width / 3
            }
            TableViewColumn {
                role: "userName"
                title: "user name"
                width: mainView.width / 3
            }
            TableViewColumn {
                role: "phoneNumber"
                title: "phone number"
                width: mainView.width / 3
            }
            model: tableModel
        }
    }
}
