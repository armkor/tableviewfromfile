#pragma once
#include <QAbstractListModel>
#include "userinfo.h"


class UserInfoModel : public QAbstractListModel
{
	Q_OBJECT
public:
	enum InfoRoles
	{
		IdRole = Qt::UserRole + 1,
		NameRole,
		PhoneNumberRole
	};

	void add(UserInfo);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	QHash<int, QByteArray> roleNames() const override;
	bool setData(const QModelIndex &index, const QVariant &value, int role) override;


private:
	QList<UserInfo> m_userInfo;
};
