#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "userinfomodel.h"
#include "filereader.h"

int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	QGuiApplication app(argc, argv);

	UserInfoModel model;
	FileReader::read(&model, ":/text.txt");

	QQmlApplicationEngine engine;
	engine.rootContext()->setContextProperty("tableModel", &model);
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	return app.exec();
}
