#pragma once
#include <QString>

class UserInfo
{
public:
	UserInfo(QString, QString, QString);

	QString id() const;
	QString name() const;
	QString phoneNumber() const;

	void setId(QString);
	void setName(QString);
	void setPhoneNumber(QString);

private:
	QString m_id;
	QString m_name;
	QString m_phoneNumber;
};
