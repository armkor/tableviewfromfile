#include "userinfo.h"

UserInfo::UserInfo(QString id_, QString name_, QString phoneNumber_)
	: m_id(id_)
	, m_name(name_)
	, m_phoneNumber(phoneNumber_)
{
}

QString UserInfo::id() const
{
	return m_id;
}

QString UserInfo::name() const
{
	return m_name;
}

QString UserInfo::phoneNumber() const
{
	return m_phoneNumber;
}

void UserInfo::setId(QString value)
{
	m_id = value;
}

void UserInfo::setName(QString value)
{
	m_name = value;
}

void UserInfo::setPhoneNumber(QString value)
{
	m_phoneNumber = value;
}
